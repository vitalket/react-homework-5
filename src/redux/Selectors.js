export const productsSliceSelector = (state) => state.productsSlice;

export const modalSliceSelector = (state) => state.modalSlice;