import { configureStore } from "@reduxjs/toolkit";
import { rememberReducer, rememberEnhancer } from "redux-remember";

import productsSlice from "./reducers/productsSlice";
import modalSlice from "./reducers/modalSlice";

const rememberedKeys = ["productsSlice"];

const store = configureStore({
  reducer: rememberReducer({
    productsSlice,
    modalSlice
  }),

  enhancers: (getDefaultEnhancers) =>
    getDefaultEnhancers().concat(
      rememberEnhancer(
        window.localStorage,
        rememberedKeys
      )
    ),
});

export default store;