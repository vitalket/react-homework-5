import React, { useEffect } from 'react';
import { Link, Outlet } from 'react-router-dom';
import { FaShoppingCart, FaStar } from "react-icons/fa";
import { getAllProducts } from '../redux/reducers/productsSlice';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';

import { productsSliceSelector } from "../redux/Selectors";

const Layout = () => {

  const {cart, favorites} = useSelector(productsSliceSelector);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllProducts());
  }, []);

  return (
    <>
      <header className="header">
        <div className="container">
          <div className="header__block">
            <div>LOGO</div>
            <nav>
              <ul>
                <li>
                  <Link to="/">Home</Link>
                </li>
              </ul>
            </nav>
            <div className="header__icons">
              <Link to="/cart">
                <FaShoppingCart />
                <span>{cart.length}</span>
              </Link>
              <Link to="/favorite">
                <FaStar />
                <span>{favorites.length}</span>
              </Link>
            </div>
          </div>
        </div>
      </header>

      <main className="main">
        <Outlet />
      </main>

      <footer className="footer">
        <div className="container">Footer</div>
      </footer>
    </>
  );
}

export { Layout }