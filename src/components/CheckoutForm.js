import React, { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useSelector, useDispatch } from "react-redux";
import { productsSliceSelector } from "../redux/Selectors";
import { removeFromCart } from "../redux/reducers/productsSlice";

const CheckoutForm = () => {
  const dispatch = useDispatch();
  const { cart } = useSelector(productsSliceSelector);
  const [orderSent, setOrderSent] = useState(false);

  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      age: "",
      address: "",
      phone: "",
    },
    validationSchema: Yup.object({
      firstName: Yup.string().required("Обов'язкове поле"),
      lastName: Yup.string().required("Обов'язкове поле"),
      age: Yup.number()
        .required("Обов'язкове поле")
        .positive("Введіть коректний вік"),
      address: Yup.string().required("Обов'язкове поле"),
      phone: Yup.string().required("Обов'язкове поле"),
    }),
    onSubmit: (values) => {
      console.log("Придбані товари:", cart);
      console.log("Інформація про користувача:", values);

      cart.forEach((product) => {
        dispatch(removeFromCart(product.id));
      });

      setOrderSent(true);

      formik.resetForm();
    },
  });

  return (
    <div>
      {orderSent ? (
        <p className="cart-products__submit">
          Замовлення відправлено на обробку. Очікуйте дзвінка менеджера.
        </p>
      ) : (
        <form className="form-checkout" onSubmit={formik.handleSubmit}>
          <div>
            <label htmlFor="firstName">Ім'я:</label>
            <input
              type="text"
              id="firstName"
              name="firstName"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.firstName}
            />
            {formik.touched.firstName && formik.errors.firstName ? (
              <div>{formik.errors.firstName}</div>
            ) : null}
          </div>
          <div>
            <label htmlFor="lastName">Прізвище:</label>
            <input
              type="text"
              id="lastName"
              name="lastName"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.lastName}
            />
            {formik.touched.lastName && formik.errors.lastName ? (
              <div>{formik.errors.lastName}</div>
            ) : null}
          </div>
          <div>
            <label htmlFor="age">Вік:</label>
            <input
              type="text"
              id="age"
              name="age"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.age}
            />
            {formik.touched.age && formik.errors.age ? (
              <div>{formik.errors.age}</div>
            ) : null}
          </div>
          <div>
            <label htmlFor="address">Адреса доставки:</label>
            <input
              type="text"
              id="address"
              name="address"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.address}
            />
            {formik.touched.address && formik.errors.address ? (
              <div>{formik.errors.address}</div>
            ) : null}
          </div>
          <div>
            <label htmlFor="phone">Мобільний телефон:</label>
            <input
              type="text"
              id="phone"
              name="phone"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.phone}
            />
            {formik.touched.phone && formik.errors.phone ? (
              <div>{formik.errors.phone}</div>
            ) : null}
          </div>
          <button type="submit">Checkout</button>
        </form>
      )}
    </div>
  );
};

export default CheckoutForm;
