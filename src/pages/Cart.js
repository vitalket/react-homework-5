import React from "react";
import { FaStar } from "react-icons/fa";
import { Modal } from "../components/Modal";
import { Button } from "../components/Button";

import { useDispatch, useSelector } from "react-redux";
import { productsSliceSelector, modalSliceSelector } from "../redux/Selectors";

import { addCart, addFavorites, removeFromCart } from "../redux/reducers/productsSlice";
import { openModal, closeModal } from "../redux/reducers/modalSlice";
import CheckoutForm from "../components/CheckoutForm";

const Cart = () => {
  const { favorites, cart, status, error } = useSelector(productsSliceSelector);

  const { isOpen, header, text, closeButton, buttons } =
    useSelector(modalSliceSelector);

  const dispatch = useDispatch();

  const handleAddToCart = (productId, name) => {
    dispatch(
      openModal({
        header: "Додати товар в кошик?",
        text: `Назва: "${name}"`,
        closeButton: true,
        buttons: [
          {
            backgroundColor: "black",
            text: "Так",
            onClick: () => {
              dispatch(addCart(productId));
              dispatch(closeModal());
            },
          },
          {
            backgroundColor: "black",
            text: "Ні",
            onClick: () => {
              dispatch(closeModal());
            },
          },
        ],
      })
    );
  };

  const handleAddToFavorites = (productId) => {
    dispatch(addFavorites(productId));
  };

  const handleRemoveFromCart = (productId, name) => {
    dispatch(
      openModal({
        header: "Видалити товар з кошика?",
        text: `Назва: "${name}"`,
        closeButton: true,
        buttons: [
          {
            backgroundColor: "black", 
            text: "Так",
            onClick: () => {
              dispatch(removeFromCart(productId));
              dispatch(closeModal());
            },
          },
          {
            backgroundColor: "black",
            text: "Ні",
            onClick: () => {
              dispatch(closeModal());
            },
          },
        ],
      })
    );
  };

  if (status === "loading") {
    return <h2>Loading...</h2>;
  }

  if (status === "failed") {
    return <h2>{error}</h2>;
  }

  if (status === "done") {
    return (
      <section className="cards-products">
        <div className="container">
          <h2 className="cards-products__title">Кошик</h2>
          <div className="cards-products__cards">
            {cart && cart.length > 0 ? (
              cart.map((product) => (
                <article
                  key={product.id}
                  className="cards-products__card card-product"
                >
                  <div className="card-product__image-box">
                    <img src={product.url} alt={product.name} />
                  </div>
                  <div className="card-product__details">
                    <h4 className="card-product__name">{product.name}</h4>
                    <p className="card-product__color">{product.color}</p>
                    <div className="card-product__buy-block">
                      <p className="card-product__price">{product.price}$</p>

                      <FaStar
                        className={`card-product__favorite-icon ${
                          favorites.findIndex(
                            (item) => item.id === product.id
                          ) > -1
                            ? "selected"
                            : ""
                        }`}
                        onClick={() => handleAddToFavorites(product.id)}
                      />

                      {cart.findIndex((item) => item.id === product.id) > -1 ? (
                        <button className="card-product__add-button card-product__add-button--inactive">
                          Product added
                        </button>
                      ) : (
                        <button
                          className="card-product__add-button"
                          onClick={() =>
                            handleAddToCart(product.id, product.name)
                          }
                        >
                          Add to cart
                        </button>
                      )}

                      <button
                        className="card-product__remove-button"
                        onClick={() =>
                          handleRemoveFromCart(product.id, product.name)
                        }
                      >
                        &#10006;
                      </button>
                    </div>
                  </div>
                </article>
              ))
            ) : (
              <p>Корзина пуста</p>
            )}
          </div>
          {cart && cart.length > 0 && (
            <>
              <div className="cart-products__checkout">Оформлення заказа</div>
              <CheckoutForm />
            </>
          )}
        </div>
        {isOpen && (
          <Modal
            header={header}
            text={text}
            closeButton={closeButton}
            buttons={buttons.map((button, index) => (
              <Button
                key={index}
                backgroundColor={button.backgroundColor}
                text={button.text}
                onClick={button.onClick}
              />
            ))}
            onClose={() => dispatch(closeModal())}
          />
        )}
      </section>
    );
  }
};

export { Cart };
